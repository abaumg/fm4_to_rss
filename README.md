# fm4 to rss

Verwende die öffentlichen Informationen von https://fm4.orf.at/ und erstelle eine
RSS-feed um einen bestimmten Autor zu folgen.

RSS-feeds:
- [Erich Moechel](https://neroburner.gitlab.io/fm4_to_rss/fm4_moechel.xml)
- [Christoph "Burstup" Weiss](https://neroburner.gitlab.io/fm4_to_rss/fm4_burstup.xml)
- [Robert Rotifer](https://neroburner.gitlab.io/fm4_to_rss/fm4_rotifer.xml)
- [Martin Blumenau](https://neroburner.gitlab.io/fm4_to_rss/fm4_blumenau.xml)

